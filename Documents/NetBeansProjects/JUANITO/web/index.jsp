<%
    if(session.getAttribute("login")!=null)
    {
        response.sendRedirect("welcome.jsp");
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/newcss.css" rel="stylesheet" type="text/css"/>
        <title>LOGIN</title>
        <script language="javascript">	
            
	function validate()
	{
            var username = document.LoginForm.txt_username; 
            var password = document.LoginForm.txt_password;
				
            if (username.value == null || username.value == "")
            {
		window.alert("DEBE INGRESAR EL USUARIO");
		username.focus();
		return false;
            }
            if (password.value == null || password.value == "")
            {
		window.alert("DEBE INGRESAR LA CONTRASEÑA ?");
		password.focus();
		return false;
            }
	}		
	</script>
    </head>
    <body>
        
            <h3 style="color:green">
                <%
                    if(request.getAttribute("RegiseterSuccessMsg")!=null)
                    {
                        out.println(request.getAttribute("RegiseterSuccessMsg"));
                    }
                %>
            </h3>
            
            <h6 style="color:red">
                <%
                    if(request.getAttribute("WrongLoginMsg")!=null)
                    {
                        out.println(request.getAttribute("WrongLoginMsg"));
                    }
                %>
            </h6>           
            
        <form class="container" method="post" action="LoginController" name="LoginForm" onsubmit="return validate();">
            <h2>LOGIN</h2>
            <div class="form-group">
              <label>Usuario</label>
              <input type="text" name="txt_username" class="form-control" id="exampleInputEmail1"  placeholder="Ingrese Usuario">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Contraseña</label>
              <input type="password" name="txt_password" class="form-control" id="exampleInputPassword1" placeholder="Ingrese Contraseña">
            </div>
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="exampleCheck1">
              <label class="form-check-label" for="exampleCheck1">Recordar contraseña</label>
            </div>
            <button type="submit" name="btn_login" class="btn btn-default">Ingrese</button>
        </form>
            
    </body>
</html>

